package com.denardisimonedbvendite.dao;

import com.denardisimonedbvendite.domain.Cliente;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteDao extends JpaRepository<Cliente, Long>{};