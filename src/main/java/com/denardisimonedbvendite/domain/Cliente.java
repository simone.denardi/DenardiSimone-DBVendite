package com.denardisimonedbvendite.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity

@Table(name = "clienti")

public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long clienteId;
    String cognome;
    Integer age;

    @OneToMany(mappedBy = "clienteId", cascade = CascadeType.ALL)
    @JsonIgnore
    Set<DettaglioVendita> dettagliovendite;
}