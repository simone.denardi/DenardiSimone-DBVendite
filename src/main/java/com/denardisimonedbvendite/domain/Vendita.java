package com.denardisimonedbvendite.domain;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Entity

@Table(name="vendite")

public class Vendita{
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    Long numeroScontrino;
    Date data;


    @OneToMany(mappedBy = "numeroScontrino" , cascade = CascadeType.ALL)
    @JsonIgnore
    Set<DettaglioVendita> dettaglivendite;
}