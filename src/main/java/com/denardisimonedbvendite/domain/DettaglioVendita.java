package com.denardisimonedbvendite.domain;

import java.text.DecimalFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Entity

@Table(name="dettaglivendite")

public class DettaglioVendita{
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    Integer riga;
    String prodotto;
    DecimalFormat importo;
    Integer numeroscontrino;
    Integer clienteid;

    @ManyToOne
    @JoinColumn(name = "clienti", nullable = false)
    Cliente cliente;
}

